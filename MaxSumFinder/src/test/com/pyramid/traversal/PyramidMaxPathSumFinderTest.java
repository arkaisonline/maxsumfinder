package test.com.pyramid.traversal;


import org.junit.Test;

import com.pyramid.traversal.PyramidMaxPathSumFinder_BottomToTop;
import com.pyramid.traversal.PyramidMaxPathSumFinder_TopToBottom;
import com.pyramid.traversal.PyramidMaxPathSumFinder_TopToBottomRecursive;

import static org.junit.Assert.assertEquals;

public class PyramidMaxPathSumFinderTest {

    private final int pyramid1MaxSumExpected = 16;
    private final int pyramid2MaxSumExpected = 8186;

	@Test
    public void testTopToBottom() {
        PyramidMaxPathSumFinder_TopToBottom pyramidMaxPathSumFinder_topToBottom = new PyramidMaxPathSumFinder_TopToBottom();

        assertEquals(pyramid1MaxSumExpected,pyramidMaxPathSumFinder_topToBottom.findMaxSum(PyramidTestData.cloneArray(PyramidTestData.pyramid1)));
		assertEquals(pyramid2MaxSumExpected,pyramidMaxPathSumFinder_topToBottom.findMaxSum(PyramidTestData.cloneArray(PyramidTestData.pyramid2)));
		
    }
	
	@Test
    public void testBottomToTop() {
        PyramidMaxPathSumFinder_BottomToTop pyramidMaxPathSumFinder_bottomToTop = new PyramidMaxPathSumFinder_BottomToTop();

        assertEquals(pyramid1MaxSumExpected,pyramidMaxPathSumFinder_bottomToTop.findMaxSum(PyramidTestData.cloneArray(PyramidTestData.pyramid1)));
		assertEquals(pyramid2MaxSumExpected,pyramidMaxPathSumFinder_bottomToTop.findMaxSum(PyramidTestData.cloneArray(PyramidTestData.pyramid2)));
    }
	@Test
    public void testRecursive() {
        PyramidMaxPathSumFinder_TopToBottomRecursive pyramidMaxPathSumFinder_topToBottomRecursive = new PyramidMaxPathSumFinder_TopToBottomRecursive();

	    assertEquals(pyramid1MaxSumExpected,pyramidMaxPathSumFinder_topToBottomRecursive.findMaxSum(PyramidTestData.cloneArray(PyramidTestData.pyramid1)));
		assertEquals(pyramid2MaxSumExpected,pyramidMaxPathSumFinder_topToBottomRecursive.findMaxSum(PyramidTestData.cloneArray(PyramidTestData.pyramid2)));
    }
	
}
