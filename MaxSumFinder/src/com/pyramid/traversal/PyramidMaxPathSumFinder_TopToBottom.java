package com.pyramid.traversal;

import java.util.Arrays;
/**
 * This class is dedicated to calculate max path sum of a given triangle (in a binary tree pattern) from top to bottom
 *  Approach : In this approach, we try to calculate max path sum for any given leaf and keeping max possible value
 *  by adding the leaf to other leaves from next row to an array and swapping them with next max value available.
 *  The max possible length of the array is the max number of columns in a row of the triangle (i;e depth of the triangle)
 *  After all iterations, the sum array would hold the max possible values for every unique nodes. And maximum value
 *  available in the array is the maximum sum possible.
 *   
 *  Additional logic : To check odd/even number 
 *  
 * @author Arka Bhattacharjee
 *
 */
public final class PyramidMaxPathSumFinder_TopToBottom {
	//Top to bottom approach
		public int findMaxSum(int[][] treeToTraverse) {
			//checking if starting point is an odd or even number
			boolean oddRoot =(treeToTraverse[0][0] % 2 != 0);
	        int size = treeToTraverse.length;
	        int[] sum = new int[size];;
	        
	        sum[0] = treeToTraverse[0][0];
	        for(int row = 1; row <= size - 1; row++){
	            int colSize = treeToTraverse[row].length;
	            for(int col = colSize - 1; col >= 0; col--) {
	            	//if starting root is odd (i:e if row=0,col=0 is an odd number, we need to select odd numbers for every row=even value and select even row is odd
	            	if(oddRoot) {
	            		if(row % 2 != 0) {
	            			if(treeToTraverse[row][col] % 2 == 0) {
	            				addNode(row, col, colSize, sum, treeToTraverse);
	            			}
	            		}else {
	            			if(treeToTraverse[row][col] % 2 != 0) {
	            				addNode(row, col, colSize, sum, treeToTraverse);
	            			}
	            		}
	            	}else { //if starting root is even (i:e if row=0,col=0 is an even number, we need to select even numbers for every row=even value and select odd when row is odd 
	            		if(row % 2 == 0) {
	            			if(treeToTraverse[row][col] % 2 == 0) {
	            				addNode(row, col, colSize, sum, treeToTraverse);
	            			}
	            		}else {
	            			if(treeToTraverse[row][col] % 2 != 0) {
	            				addNode(row, col, colSize, sum, treeToTraverse);
	            			}
	            		}
	            	}
	            }
	        }
	        Arrays.sort(sum);
	        return sum[sum.length-1];
	    }
		
		//helper method to add node values
		private static void addNode(int row, int col, int colSize, int[] sum, int[][] triangle) {
			if (col == 0) {
	            sum[0] = sum[0] +triangle[row][col];
	        } else if (col == (colSize - 1)  && sum[col-1] > 0) {
	            sum[col] = sum[col-1] + triangle[row][col];
	        } else if(sum[col-1] > 0){
	            sum[col] = Math.max(sum[col-1], sum[col]) + triangle[row][col];
	        }
		}
}
