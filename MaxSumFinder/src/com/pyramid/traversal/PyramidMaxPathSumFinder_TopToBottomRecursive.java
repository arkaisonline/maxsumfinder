package com.pyramid.traversal;
/**
 * This class is dedicated to calculate max path sum of a given triangle (in a binary tree pattern) from top to bottom
 * in a recursive approach
 *
 *  
 *  Additional logic : To check odd/even number 
 * 
 * @author Arka Bhattacharjee
 *
 */
public final class PyramidMaxPathSumFinder_TopToBottomRecursive {
		
		public int findMaxSum (int[][] treeToTraverse) {
			//checking if starting node is odd or even
			boolean oddRoot = (treeToTraverse[0][0] % 2 != 0);
			return maxSumRecursion(treeToTraverse, 0, 0, 0, Integer.MIN_VALUE, oddRoot);  
		}
		 private static int maxSumRecursion(int[][] triangle, int row, int column, int sum, int maxSum,boolean oddRoot) {  
			 //if starting node is an odd
			if(oddRoot) {
				if (row % 2 == 0) {
					if (triangle[row][column] % 2 != 0) {
						sum += triangle[row][column];
					}
				} else {
					if (triangle[row][column] % 2 == 0) {
						sum += triangle[row][column];
					}
				}
			}else {//if starting node is even
				if (row % 2 == 0) {
					if (triangle[row][column] % 2 == 0) {
						sum += triangle[row][column];
					}
				} else {
					if (triangle[row][column] % 2 != 0) {
						sum += triangle[row][column];
					}
				}
			}
			
			if (row == triangle.length - 1) { // last row
				if (sum > maxSum)
					return sum;
			} else {
				maxSum = maxSumRecursion(triangle, row + 1, column, sum, maxSum, oddRoot);
				maxSum = maxSumRecursion(triangle, row + 1, column + 1, sum, maxSum, oddRoot);
			}

			return maxSum;  
		 }  
		 
}
