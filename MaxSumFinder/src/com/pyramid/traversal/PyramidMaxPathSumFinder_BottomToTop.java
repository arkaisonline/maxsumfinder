package com.pyramid.traversal;

/**
 * This class is dedicated to calculate max path sum of a given triangle (in a binary tree pattern) from bottom to top
 * Approach -
 * Given triangle : 
 *  1         
 *  2 3
 *  4 5 6
 *  7 8 9 10
 *  After first iteration : 
 *  1
 *  2 3
 *  max((4+7),(4+8)) : 12 max((5+8),(5+9)) : 14  max((6+9),(6+10)) : 16
 *  1
 *  2 3
 *  12 14 16 
 *  Next iteration : repeat : 
 *  1
 *  max((2+12),(2+14)):16 max((3+14),(3+16)) :19
 *  1
 *  16 19 
 *  Next iteration : repeat 
 *  max( (1+16), (1+19)) : 20
 *  Answer : 20 
 *  
 *  Additional logic : To check odd/even number 
 *  if first node (in this case 1) is odd and depth of triangle is even (in this case 4)
 *  for every iteration i, if i is even choose an odd number : if i is odd, choose an even number - ignore others
 *  Value of the current starting node is read from a temp array (which is same as the given triangle), 
 *  as the value of current node in original triangle will get replaced in
 *  every iteration thus making it impossible to know whether it was an odd number or even
 * @author Arka Bhattacharjee
 *
 */
public final class PyramidMaxPathSumFinder_BottomToTop {

	   
	//traversing from bottom to top
	public int findMaxSum(int[][] treeToTraverse) {
		//variable to check what is the starting number : odd or even
		boolean oddRoot =(treeToTraverse[0][0] % 2 != 0);
		int[][] temp = treeToTraverse;
		int depth = treeToTraverse.length;
		for (int i = depth - 2; i >= 0; i--) {//starting pointer at depth -2 : i at 13th
			for (int j = 0; j < treeToTraverse[i].length; j++) {
				int leftNode = treeToTraverse[i + 1][j];
				int rightNode = treeToTraverse[i + 1][j + 1];
				int currentNode = temp[i][j];
				int max = 0;
				if(oddRoot) {
					//if i position is odd : current node to be selected is even, if i position is even, current node to be selected should be an odd
					if ( (i % 2 == 0 && currentNode % 2 != 0) || (i % 2 != 0 && currentNode % 2 == 0)) {
						if((i == (depth - 2))) {//first iteration : choose next row as even or odd based on depth 
							if(isOdd(depth)) {
								if(isOdd(leftNode) && isOdd(rightNode)) {
									max = Math.max(leftNode, rightNode);
								}else if(isOdd(leftNode)) {
									max = leftNode;
								}else if(isOdd(rightNode)) {
									max = rightNode;
								}
							}else {
								if(isEven(leftNode) && isEven(rightNode)) {
									max = Math.max(leftNode, rightNode);
								}else if(isEven(leftNode)) {
									max = leftNode;
								}else if(isEven(rightNode)) {
									max = rightNode;
								}
							}
							
						}else {
							max = Math.max(leftNode, rightNode);
						}
						treeToTraverse[i][j] = treeToTraverse[i][j] + max;
						
					}else {
						//making all other nodes 0 so that they don't change any subsequent values
						treeToTraverse[i][j] =0;
					}
				}else {
					//if i position is even : current node to be selected is even, if i position is odd, current node to be selected should be an odd
					if ( (i % 2 == 0 && currentNode % 2 == 0) || (i % 2 != 0 && currentNode % 2 != 0)) {
						if((i == (depth - 2))) {
							if(isOdd(depth)) {
								if(isEven(leftNode) && isEven(rightNode)) {
									max = Math.max(leftNode, rightNode);
								}else if(isEven(leftNode)) {
									max = leftNode;
								}else if(isEven(rightNode)) {
									max = rightNode;
								}
							}else {
								if(isOdd(leftNode) && isOdd(rightNode)) {
									max = Math.max(leftNode, rightNode);
								}else if(isOdd(leftNode)) {
									max = leftNode;
								}else if(isOdd(rightNode)) {
									max = rightNode;
								}
							}
						}else {
							max = Math.max(leftNode, rightNode);
						}
						treeToTraverse[i][j] = treeToTraverse[i][j] + max;
					}else {
						//making all other nodes 0 so that they don't change any subsequent values
						treeToTraverse[i][j] =0;
					}
				}
				
			}
		}
		return treeToTraverse[0][0];
	}
	private static boolean isEven (int number) {
		return (number % 2 == 0);
	}
	private static boolean isOdd (int number) {
		return (number % 2 != 0);
	}
}
