# MaxSumFinder
Given a triangle of integers, this program finds out max possible sum of a path
traversing from top to bottom by selecting next element in a tree pattern. 
Condition : if the root node is even, child selected must be odd and vice versa.

In this project the problem is solved in below 3 different ways - 
1. Top to bottom approach  : in this approach, we try to find the max sum for a 
given position by adding the next nodes and cmparing both.
2. Top to bottom using recursion : in this approach as well we try to find the 
max sum from top to bottom but using recursion.
3. Bottom to top approach : In this approach we try to find the max value from 
bottom to top by starting from n-1 row and add left/right node of nth row.

Recomended approach : Bottom to top approach. If the triangle is big, there will
be performance impact with first 2 options as they try and find the max by
traversingn all possible paths. If the depth of the triangle is n, there will
be 2^(n-1) possible paths to traverse in top to bottom approaches which can 
take a huge amount of time if n is large.

